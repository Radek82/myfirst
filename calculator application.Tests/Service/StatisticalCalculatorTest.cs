﻿using System;
using System.Collections.Generic;
using calculator_application.Models;
using calculator_application.Service.StatisticalCalculator;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace calculator_application.Tests.Service
{
    [TestClass]
    public class StatisticalCalculatorTest
    {
        private IStatisticalCalculator _statisticalCalculator;
        public StatisticalCalculatorTest()
        {
            _statisticalCalculator = new StatisticalCalculator();
        }

        [TestMethod]
        public void AddNumberToList()
        {

            var numbersList = new List<Operation>();

            _statisticalCalculator.AddNumbers(12345, ref numbersList);

            Assert.IsTrue(numbersList.Count > 0);
        }


        [TestMethod]
        public void ClearListOfNumbers()
        {


            var numbersList = new List<Operation>
            {
                new Operation {Number = 312312312, OperationType = "+"},
                new Operation { Number = 43242, OperationType = "+"}
            };

            _statisticalCalculator.ClearNumbers(ref numbersList);

            Assert.IsTrue(numbersList.Count == 0);
        }
    }
}
