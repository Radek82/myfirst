﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(calculator_application.Startup))]
namespace calculator_application
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
