﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using calculator_application.Models;

namespace calculator_application.Service.StatisticalCalculator
{
    public class StatisticalCalculator : IStatisticalCalculator
    {
        public void AddNumbers(double number, ref List<Operation> listOfNumber)
        {
            if (listOfNumber == null)
                listOfNumber = new List<Operation>();

            var operation = new Operation
            {
                Number = number,
                OperationType = "+" 
            };

            listOfNumber.Add(operation);
        }

        public void ClearNumbers(ref List<Operation> operations)
        {
            operations.Clear();
        }
    }
}