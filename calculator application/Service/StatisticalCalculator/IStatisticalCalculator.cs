﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using calculator_application.Models;

namespace calculator_application.Service.StatisticalCalculator
{
    public interface IStatisticalCalculator
    {
        void AddNumbers(double number, ref List<Operation> listOfNumber);
        void ClearNumbers(ref List<Operation> operations);
    }
}
