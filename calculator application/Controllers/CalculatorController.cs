﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using calculator_application.Models;
using calculator_application.Service.StatisticalCalculator;

namespace calculator_application.Controllers
{
    public class CalculatorController : Controller
    {
        private readonly string _cacheKey = "CacheKey";
        private IStatisticalCalculator _statisticalCalculator;
        public CalculatorController()
        {
         _statisticalCalculator = new StatisticalCalculator();   
        }
        // GET: Calculator
        public ActionResult Index()
        {
            HttpContext.Cache.Insert(_cacheKey, new List<Operation>());

            return View();
        }

        [HttpPost]
        public ActionResult Calculate(double number, string add, string clear)
        {
            var operations = HttpContext.Cache.Get(_cacheKey) as List<Operation> ?? new List<Operation>();

            if (!string.IsNullOrEmpty(add))
            {
                _statisticalCalculator.AddNumbers(number, ref operations);

                HttpContext.Cache.Insert(_cacheKey, operations);
            }
            if (!string.IsNullOrEmpty(clear))
            {
                _statisticalCalculator.ClearNumbers(ref operations);

                HttpContext.Cache.Insert(_cacheKey, operations);
            }

            return View(operations);

        }
    }
}