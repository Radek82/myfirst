﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace calculator_application.Models
{

    public class Operation
    {
        public string OperationType { get; set; }
        public double Number { get; set; }
    }
}